package com.astro.service;
/**
 * 
 * @author shashank
 *
 */
public interface OperationService {
/**
 * Method Name : createProfile
 * 		To create signUP user.
 * @param userName
 * @return integer
 */
public int createProfile(String userName);
/**
 * Method Name : addFriend
 * 			To Add friend to a list
 * @param userName
 * @param friendName
 * @return
 */
public int addFriend(String userName,String friendName);
/**
 * Method Name : removeFriend
 * 			To Remove friend from the List
 * @param userName
 * @param friendName
 * @return
 */
public int removeFriend(String userName,String friendName);
/**
 * Method Name : viewFriend
 *    To view friend .
 * @param userName
 * @return
 */
public int viewFriend(String userName);
/**
 * Method Name : viewConnectionDistance.
 * 				to View friend of friend at given distance.
 * @param userName
 * @param dist
 * @return
 */
public int viewConnectionDistance(String userName,int dist);
/**
 * 
 * @param userName
 * @param dist
 * @return
 */
public int findAtDist(String userName,int dist);
}
//InterFace Ends.