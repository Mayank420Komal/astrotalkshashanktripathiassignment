package com.astro.serviceImpl;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

import com.astro.hashing.*;
import com.astro.service.OperationService;

public class OperatinServiceImpl implements OperationService{

	HashingClass objHash=new HashingClass();
	public int createProfile(String userName) {
		if(objHash.friendList.containsKey(userName))
		{
			//Return -1 if user already exist.
			return -1;
		}

		if(objHash.friendList.get(userName)==null) {
			objHash.friendList.put(userName, new LinkedList<String>());
		}
		//Return 1 if Successfully added to list.
		return 1;
	}

	public int addFriend(String userName, String friendUserName) {
		if(!objHash.friendList.containsKey(userName))
		{
			//Return -1 if user with this userName does not exist.
			return -1;
		}
		if(!objHash.friendList.containsKey(friendUserName))
		{

			//Return -2 if friend with this userName does not exist.
			return -2;
		}
		objHash.friendList.get(userName).add(friendUserName);
		objHash.friendList.get(friendUserName).add(userName);
		//Return 1 if friend is Successfully Added.
		return 1;
	}

	public int removeFriend(String userName, String friendUserName) {
		if(!objHash.friendList.containsKey(userName))
		{
			//Return -1 if user with this userName does not exist.
			return -1;
		}
		if(!objHash.friendList.containsKey(friendUserName))
		{

			//Return -2 if friend with this userName does not exist.
			return -2;
		}
		//Removing friend from both the list.
		objHash.friendList.get(userName).remove(friendUserName);
		objHash.friendList.get(friendUserName).remove(userName);
		
		//Return 1 if successfully removed.
		return 1;
	}

	public int viewFriend(String userName) {
		if(!objHash.friendList.containsKey(userName))
		{
			//Return -1 if user with this userName does not exist.
			return -1;
		}
		LinkedList<String>friendList=objHash.friendList.get(userName);
		for(String frindName:friendList)
		{
			System.out.println(" "+frindName);
		}
		//Return 1 after Successfully Printing friend Name.
		return 1;
	}

	public int viewConnectionDistance(String userName, int dist) {
		if(!objHash.friendList.containsKey(userName))
		{
			//Return -1 if user with this userName does not exist.
			return -1;
		}

		findAtDist(userName,dist);
		//Return if Successfully found out friend at given distance.
		return 1;
	}

/**
 * Method Name : findAtDist
 * This Method Uses Breadth First search to find People at given Distance.
 */
	public int findAtDist(String userName,int dist)
	{
		if(dist==0)
		{
			System.out.println("No friedn at distance Zero");
			return 0;
		}
		//to Store Node which are already visited.
		HashSet<String>visitedNode =new HashSet<String>();
		//to Store a friend of friend node at distance dist.
		ArrayList<String>frndAtDist=new ArrayList<String>();
		//queue to do Breadth first search.
		LinkedList<String>que=new LinkedList<String>();
		que.add(userName);
		int currentDist=0;
		while(true)
		{
			int len=que.size();
			int itr=0;
			while(itr<len)
			{
				String name=que.getFirst();
				que.remove(name);
				visitedNode.add(name);
				LinkedList<String>connectedNode=objHash.friendList.get(name);
				for(String adjNode:connectedNode) {
					if(visitedNode.contains(adjNode))
					{
						continue;
					}
					else
					{
						if(currentDist+1==dist)
						{
							frndAtDist.add(adjNode);
						}
						que.add(adjNode);
					}
				}
				itr++;
			}
			//Break from while loop if queue is Empty.
			if(que.size()==0)
			{
				break;
			}
			currentDist++;
		}
		
		//Printing 
		System.out.println("Friend of frinds At distance "+dist+" Are");
		for(String friendName:frndAtDist) {
			System.out.println("Name "+friendName);
		}
		//Return 1 if successfully printed
		return 1;
	}

}//Class End
